# Arjun Subramonian & Ishani Karmarkar
# A Novel Approach to Retransmission Timeout (RTO) Calculation Through Online Multivariate Polynomial Regression and Outlier-Based Anomaly Detection  

from rttpredictor import RTTPredictor
import numpy as np
import datetime

"""IGNORE OPTIMIZATION FAILURE WARNINGS"""
import warnings
warnings.filterwarnings('error')

class RegionBased(RTTPredictor):

    # n = 25, n_buff = 35, alpha_rto = 0.003, alpha_lam = 0.1, delta = 600, lam = 0, min_rto = 1

    def __init__(self, under_error=1, over_error=-1, n = 25, n_buff = 35, alpha_rto = 0.003, alpha_lam = 0.1, delta = 600, lam = 0, min_rto = 1, RMSD = False):
        RTTPredictor.__init__(self, under_error, over_error)

        """IMPORTANT CONSTANTS"""
        # number of previous RTTs in x vector
        self.n = n
        # number of previous lambdas in x buffer vector
        self.n_buff = n_buff

        # learning rate of online algorithm (how quickly adapts to changing conditions)
        # LR for predicted RTT
        self.alpha_rto = alpha_rto
        # LR for predicted RTT --> RTO conversion
        self.alpha_lam = alpha_lam

        # value to trick algorithm into producing optimal overstimate for RTT --> RTO conversion
        self.delta = delta
        # add to initial predicted RTO, output of buffer function
        self.lam = lam

        self.counter = 0
        self.init_rto = min_rto

        self.RMSD = RMSD
        self.init_err = 0
        # self.init_err = []

        """X VECTOR (ROW OF MATRIX OF INDEPEDENT VARIABLES) THAT IS CURRENTLY BEING PROCESSED"""
        self.curr_x = [0 for i in range(0, self.n + 1)]
        """CURRENT Y VALUE"""
        self.curr_y = min_rto
        self.theta = np.zeros((n + 1, 1))

        """THE MIN AND MAX OF EACH COLUMN"""
        self.min = [0 for i in range(0, self.n + 1)]
        self.max = [0 for i in range(0, self.n + 1)]

        """FOR BUFFER FUNCTION"""
        self.x_buff = [0 for i in range(0, self.n_buff + 1)]
        self.theta_buff = np.zeros((n_buff + 1, 1))

        self.min_buff = [0 for i in range(0, self.n_buff + 1)]
        self.max_buff = [0 for i in range(0, self.n_buff + 1)]

    def update_rto(self, rtt, time):

        """DEFAULT VALUE OF FIRST COLUMN IS ONE"""
        self.curr_x[0:1] = [1]
        self.curr_y = rtt

        self.x_buff[0:1] = [1]
        if self.RMSD:
            self.init_err += abs(self.init_rto - rtt) ** 2
            # self.init_err.append(abs(self.init_rto - rtt))
            self.delta = np.sqrt(self.init_err / (self.counter + 1))
        self.lam = (rtt + self.delta) / self.init_rto

        """MAIN"""
        copy = self.curr_x[:]
        self.curr_x = self.feature_scale(np.array(self.curr_x), self.min, self.max)
        self.theta = self.update_theta(self.curr_x, self.curr_y, self.theta, self.alpha_rto)

        """LAM"""
        copy_buff = self.x_buff[:]
        self.x_buff = self.feature_scale(np.array(self.x_buff), self.min_buff, self.max_buff)
        self.theta_buff = self.update_theta(self.x_buff, self.lam, self.theta_buff, self.alpha_lam)

        """LAM"""
        add_val = self.lam
        self.x_buff = [1, add_val] + [val for val in copy_buff[1:-1]]
        copy_buff = self.x_buff[:]
        self.x_buff = self.feature_scale(np.array(self.x_buff), self.min_buff, self.max_buff)
        self.lam = self.fun(self.x_buff, self.theta_buff)
        self.x_buff = copy_buff[:]

        """MAIN"""
        add_val = rtt
        self.curr_x = [1, add_val] + [val for val in copy[1:-1]]
        copy = self.curr_x[:]
        self.curr_x = self.feature_scale(np.array(self.curr_x), self.min, self.max)
        self.init_rto = self.fun(self.curr_x, self.theta)
        if self.lam > 1:
            self.rto = self.init_rto * self.lam
        else:
            self.rto = self.init_rto
        self.curr_x = copy[:]

        self.counter += 1
        return self.rto

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        return self.update_rto(rtt, time)

    def feature_scale(self, x, mini, maxi): 
        """1 for vertical displacement flexibility"""
        for i in range(1, len(x)):
            if mini[i] == 0 or x[i] < mini[i]:
                mini[i] = x[i]
            if maxi[i] == 0 or x[i] > maxi[i]:
                maxi[i] = x[i]
        try:
            return np.divide(np.subtract(x, mini), np.subtract(maxi, mini))
        except RuntimeWarning:
            diff = np.subtract(maxi, mini)
            for i in range(0, len(diff)):
                if diff[i] == 0:
                    diff[i] = 1
            return np.divide(np.subtract(x, mini), diff)

    def fun(self, x, theta): 
        x = np.matrix(x)
        theta = np.matrix(theta)
        return np.dot(x, theta).item(0)
        
    def update_theta(self, x, y, theta, alpha):
        x = np.matrix(x)
        return np.subtract(theta, np.multiply(x.T, alpha * (self.fun(x, theta) - y)))

