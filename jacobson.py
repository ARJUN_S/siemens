# Congestion Avoidance and Control

from rttpredictor import RTTPredictor

class Jacobson(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, alpha=1/8, beta=1/4, min_rto = 1000):
        """Initializes the Jacobson algorithm (RFC6298 -- THIS IS IMPORTANT TO INCLUDE IN PAPER WHEN DISCUSSING JACOBSON'S ALGORITHM,
            EXPLAINS FROM WHERE ALL THE "MAGICAL" CONSTANTS ORIGINATE)."""
        RTTPredictor.__init__(self, under_error, over_error)
        self.srtt = None
        self.rttvar = None
        self.k = 4
        self.b = beta
        self.a = alpha

        self.min_rto = min_rto

        # self.init_err = 0

    def update_rto(self, rtt, time):
        if self.srtt is None:
            self.srtt = rtt
            self.rttvar = rtt/2
            self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
        else:
            self.rttvar = (1 - self.b) * self.rttvar + (
                self.b * abs(self.srtt - rtt))
            self.srtt = (1 - self.a) * self.srtt + self.a * rtt
            self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
        
        # self.init_err += abs(self.srtt - rtt) ** 2

        if self.rto < self.min_rto:
            self.rto = self.min_rto
        return self.rto

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        self.rto = min(2*self.rto, 60*1000)
        if self.rto < self.min_rto:
            self.rto = self.min_rto
        return self.rto
    """^^^THE ALGORITHM GENERALLY DRASTICALLY OVERESTIMATES THE RTO"""
    """You can make the claim that, because Jacobson's algorithm regularly falls back on 1000 ms, as more packets are processed,
    the greater the number of "anomaly packets, the greater the packet loss, but RegionBased adapts."""


