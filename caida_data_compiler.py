# Arjun Subramonian & Ishani Karmarkar

from fileinput import input 
import numpy as np 
import scipy.optimize as sp
import datetime

def feature_scale(X): 
	for col in range(0, X.shape[1]):
		mean = np.mean(X[:, col])
		stdev = np.std(X[:, col])
		if stdev == 0:
			stdev = 1
		X[:, col] = np.divide(np.subtract(X[:, col], mean), stdev)

def weight(X, n):
	i = n
	for col in range(2, X.shape[1]):
		X[:, col] = np.multiply(X[:, col], i / (2.5 * n))
		i -= 1

def cost_function(theta, X, Y, rho, lam): 
	m = Y.shape[0]
	J = np.sum(np.square(np.dot(X, theta).T - Y)) / (2 * m) + lam / (2 * m) * np.sum(np.square(np.matrix(theta[1:]).T))
	return J

def grad(theta, X, Y, rho, lam):
	m = Y.shape[0]
	grad = np.dot(X.T, (np.dot(X, theta).T - Y)) / m + lam / m * np.concatenate((np.matrix(np.zeros((1, 1))), np.matrix(theta[1:]).T))
	return np.ravel(grad)

def calc_theta(X, Y, theta, rho, lam = 0.3):
	return sp.minimize(cost_function, theta, args = (X, Y, rho, lam), method='BFGS', jac=grad)

counter = 0 
n = 30 

for line in input():
	counter += 1 

X = np.matrix(np.zeros((counter, n + 2)))
Y = np.matrix(np.zeros((counter, 1)))
theta = np.matrix(np.ones((n + 2, 1)))

with open('coeffs.txt', 'r') as f:
	for line in f:
		arr = line.strip().split()
		for i in range(0, len(arr)):
			theta[0, i] = arr[i]

count = 0 
for line in input(): 
	rtt, time = line.strip().split()

	Y[count, 0] = rtt
	X[count, 0] = 1
	X[count, 1] = time
	pos = count - 1
	idx = 2
	while pos >= 0 and pos >= count - n:
		X[count, idx] = Y[pos, 0]
		pos -= 1
		idx += 1
	count += 1

feature_scale(X)
weight(X, n)
theta = np.ravel(calc_theta(X, Y, theta, counter).x)

for i in range(0, theta.size):
	print theta[i],
	