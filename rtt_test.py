'''Arjun Subramonian'''

from region_based import RegionBased 
from jacobson import Jacobson

import fileinput
import datetime
import numpy as np

if __name__ == "__main__":
    predictors = [
        RegionBased(RMSD = True),
        Jacobson()
    ]

    data = [['number of packets', 'MPR packet loss', 'MPR median error', "Jacobson's packet loss", "Jacobson's median error"]]

    count = 0
    least = 1000
    least_count = 0
    for line in fileinput.input():
        arr = line.strip().split()
        rtt = float(arr[0])
        try:
            time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        except:
            continue
        for p in predictors:
            p.receive_rtt(rtt, time)
        count += 1

        if (count + 1) % 10000 == 0:

            temp = [str(count + 1)]

            print()
            print('Count:', count + 1)
            print('Least:', least, '--', least_count)
            print()

            for p in predictors:

                temp.append(str(p.lost_packets*100.0/(count + 1)))
                temp.append(str(np.median(p.error)))

                print(type(p).__name__)
                print('Percent packet loss: ', p.lost_packets*100.0/(count + 1))
                print ('Typical (median error): ', np.median(p.error))
                print()

            data.append(temp)

        if (count + 1) >= 10000 and predictors[0].lost_packets*100.0/(count + 1) - predictors[1].lost_packets*100.0/(count + 1) < least:
            least = predictors[0].lost_packets*100.0/(count + 1) - predictors[1].lost_packets*100.0/(count + 1)
            least_count = count + 1
        # if count + 1 == 20000:
            # break

    temp = [str(count + 1)]
    for p in predictors:
        temp.append(str(p.lost_packets*100.0/(count + 1)))
        temp.append(str(np.median(p.error)))
    data.append(temp)

    print('FINAL:')
    for p in predictors:
        print(type(p).__name__)
        print('Percent packet loss: ', p.lost_packets*100.0/(count + 1))
        print ('Typical (median error): ', np.median(p.error))
        print()

    with open('data_RMSD_alt.csv', 'w') as f:
        for row in data:
            f.write(','.join(row) + '\n')
    
