import fileinput as fi

for line in fi.input():
    if line.startswith("#") or line.strip() == "":
        continue
    arr = line.split()
    if arr[6] == "R" and arr[7] != "0" and arr[10] == "S":
        print arr[7], arr[5]
    for perhop in arr[13:]:
        if perhop == 'q':
            continue
        indiv = perhop.split(';');
        for elem in indiv:
            elemarr = elem.split(',')
            print elemarr[1], arr[5]