'''Arjun Subramonian'''
    
import datetime

class RTTPredictor(object):
    def __init__(self, under_error = 1, over_error = -1, min_rto = 1000):
        """Initiates the algorithm's parameters with default values"""
        self.under_error = under_error
        self.over_error = over_error
        self.rto = min_rto
        self.error = []
        self.lost_packets = 0
        # timer granularity in milliseconds
        self.granularity = 100
        
    def receive_rtt(self, rtt, time):
        """Receive the next measured RTT and update internal parameters."""
        d = self.rto - rtt
        self.error.append(abs(d))
        """IF RTO IS LESS THAN RTT -- UNDERESTIMATE or IF RTO IS GREATER THAN RTT -- OVERESTIMATE"""
        if d < 0:
            self.process_loss(rtt, time)
        else:
            self.update_rto(rtt, time)

    def update_rto(self, rtt, time):
        self.rto = rtt

    def process_loss(self, rtt, time):
        self.lost_packets += 1

