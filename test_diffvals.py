import fileinput
import datetime

import numpy as np
import scipy.optimize as sp

from region_based import RegionBased
from jacobson import Jacobson

err = []
pl = []

def run(params):
    global err
    global pl

    """GOT TO MAKE SURE THIS PROGRAM IS WORKING!!! CHECK TO SEE THAT PARAMS ACTUALLY CONTAINS OPTIMAL (OR EVEN DIFFERENT VALUES)
    THAN FROM WHEN PARAMS WAS SUBMITTED TO THE RUN FUNCTION"""
    print(params)

    """CONSTRAINTS ON OPTIMIZATION"""
    if params[0] < 5:
        params[0] = 5
    if params[1] < 5:
        params[1] = 5
    if params[2] < 0.0001:
        params[2] = 0.0001
    if params[3] < 0.0001:
        params[3] = 0.0001
    if params[2] > 0.1:
        params[2] = 0.1
    if params[3] > 0.1:
        params[3] = 0.1

    p = RegionBased(n = int(round(params[0])), n_buff = int(round(params[1])), alpha_rto = params[2], alpha_lam = params[3], delta = params[4], lam = int(round(params[5])), min_rto = int(round(params[6])))
    jac = Jacobson()

    count = 0
    """CAN THE USER OPTIMIZE THE PARAMETERS USING A RECENT SET OF RTT VALUES EACH TIME HE OR SHE EMPLOYS THE ALGORITHM?"""
    with open("tcp_exchange.dat") as f:
        for line in f:
            arr = line.strip().split()
            rtt = float(arr[0])
#            try:
#                rtt = float(arr[-1])
#            except:
#                continue
            """SIMPLY PROCESS THE TIME VALUES FROM THE FILE"""
            try:
                time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
                # time = int(arr[-2])
            except:
                continue
            p.receive_rtt(rtt, time)
            jac.receive_rtt(rtt, time)
            count += 1
            """THE MORE RTTS SAMPLED, THE BETTER -- CHANGE SENTINEL VALUE BELOW ACCORDINGLY"""
            # if count == 20000:
                # break

    med = np.median(p.error) - np.median(jac.error)
    err.append(med)
    pl.append(p.lost_packets*100.0/count - jac.lost_packets*100.0/count)

    # old cost function: p.lost_packets*100.0/count + (p.err_over / p.overs + p.err_under / p.unders) / 100
    print()
    print("Percent packet loss diff:", str((p.lost_packets*100.0/count - jac.lost_packets*100.0/count)) + "%")
    print("Over standard error - RegionBased:", str(p.err_over / p.overs))
    print("Over standard error - Jacobson's:", str(jac.err_over / jac.overs))
    print ('Typical (median error) - RegionBased: ', med)
    print ("Typical (median error) - Jacobson's: ", np.median(jac.error))
    print()

    std_err = np.std(err)
    if std_err == 0:
        std_err = 1
    std_pl = np.std(pl)
    if std_pl == 0:
        std_pl = 1

    return (med - np.mean(err)) / std_err + (p.lost_packets*100.0/count - jac.lost_packets*100.0/count - np.mean(pl)) / std_pl
    
params = [27, 34, 0.002578, 0.0040998, 600, 150, 1000]
params = np.ravel(sp.minimize(run, params, method='Powell').x)
print(params)