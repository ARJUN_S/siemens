'''so if you send me the numbers for packet loss, 
error for each constant as you change the value for that constant, 
then I can make those graphs. 
i'm thinking 1 graph for each of the constants since as you mentioned 
they are mostly self explanatory. 
and then, maybe 2-3 for delta depending on how many increments we want to do? 
maybe show the effect of using RMSD instead, etc.?'''

'''Arjun Subramonian'''

from region_based import RegionBased 
from jacobson import Jacobson

import fileinput
import datetime
import numpy as np

def run(val, n = 25, n_buff = 35, alpha_rto = 0.003, alpha_lam = 0.1, delta = 600):
    predictors = [
        RegionBased(n = n, n_buff = n_buff, alpha_rto = alpha_rto, alpha_lam = alpha_lam, delta = delta),
        Jacobson()
    ]

    count = 0
    for line in fileinput.input():
        arr = line.strip().split()
        rtt = float(arr[0])
        try:
            time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        except:
            continue
        for p in predictors:
            p.receive_rtt(rtt, time)
        count += 1
        
        if (count + 1) % 10000 == 0:
            print()
            print('Count:', count + 1)
            print()
            for p in predictors:
                print(type(p).__name__)
                print('Percent packet loss: ', p.lost_packets*100.0/(count + 1))
                print ('Typical (median error): ', np.median(p.error))
                print()

    temp = []
    temp.append(str(val))
    for p in predictors:
        temp.append(str(p.lost_packets*100.0/(count + 1)))
        temp.append(str(np.median(p.error)))

    return temp

'''data = [['n_x', 'MPR packet loss', 'MPR typical error', 'Jac packet loss', 'Jac typical error']]
for n_x in range(5, 53, 3):
    data.append(run(n_x, n = n_x))
with open('data_n_x.csv', 'w') as f:
    for row in data:
        f.write(','.join(row) + '\n')

data = [['n_omega', 'MPR packet loss', 'MPR typical error', 'Jac packet loss', 'Jac typical error']]
for n_omega in range(5, 53, 3):
    data.append(run(n_omega, n_buff = n_omega))
with open('data_n_omega.csv', 'w') as f:
    for row in data:
        f.write(','.join(row) + '\n')'''

data = [['alpha_x', 'MPR packet loss', 'MPR typical error', 'Jac packet loss', 'Jac typical error']]
for alpha_x in [0.00003, 0.0001, 0.0003, 0.001, 0.003, 0.01]:
    print('alpha_x:', alpha_x)
    data.append(run(alpha_x, alpha_rto = alpha_x))
with open('data_alpha_x.csv', 'w') as f:
    for row in data:
        f.write(','.join(row) + '\n')

data = [['alpha_omega', 'MPR packet loss', 'MPR typical error', 'Jac packet loss', 'Jac typical error']]
for alpha_omega in [0.001, 0.003, 0.01, 0.03, 0.1, 0.3]:
    print('alpha_omega:', alpha_omega)
    data.append(run(alpha_omega, alpha_lam = alpha_omega))
with open('data_alpha_omega.csv', 'w') as f:
    for row in data:
        f.write(','.join(row) + '\n')

data = [['delta (inc 100)', 'MPR packet loss', 'MPR typical error', 'Jac packet loss', 'Jac typical error']]
for delta in range(100, 1100, 100):
    print('delta (inc 100):', delta)
    data.append(run(delta, delta = delta))
with open('data_delta_inc_100.csv', 'w') as f:
    for row in data:
        f.write(','.join(row) + '\n')

data = [['delta (inc 25)', 'MPR packet loss', 'MPR typical error', 'Jac packet loss', 'Jac typical error']]
for delta in range(100, 1025, 25):
    print('delta (inc 25):', delta)
    data.append(run(delta, delta = delta))
with open('data_delta_inc_25.csv', 'w') as f:
    for row in data:
        f.write(','.join(row) + '\n')
    
