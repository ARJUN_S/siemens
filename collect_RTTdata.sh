cd ./team-1/

for dir in */; do
    cd $dir
    for file in cycle-*/*.warts.gz; do
        region=`echo "$file" | cut -d'.' -f6`
        mkdir -p ../$region/
        mv $file $file.Z
        mv $file ../$region/
    done
    cd ..
done
for dir in +([0-9])/; do
    rmdir -rf $dir
done

for dir in */; do
    cd $dir
    echo -n > tcp_exchange.dat
    for file in *.warts.gz.Z; do
        zcat $file | sc_analysis_dump | python ../get_RTTdata.py >> tcp_exchange.dat
    done
    python ../caida_data_compiler.py tcp_exchange.dat > ../coeffs.txt
    cd ..
done